#ifndef V1_1_CMAP_H
#define V1_1_CMAP_H

#include "entities.h"   //also includes coords.h, config.h
#include <list>
#include <vector>

/** This class represents a map ( game plan )
 * with all game objects and entities ( attacker / tower / path / wall etc .. )
 */
class CMap {
protected:
    int                          m_width;
    int                          m_height;
    vector < vector<CEntity *> > m_map;           ///all game objects in 2D vector
    vector < CEntrance * >       m_entrances;     ///entrances that are currently in the map
    vector < CTower * >          m_towers;        ///towers that are currently in the map
    vector < CAttacker * >       m_attackers;     ///attackers that are currently in the map
    CExit *                      m_exit;          ///exit that are currently in the map
    bool                         isWall           ( char symbol ) const ;
    list<TCoord>                 pathToEnd        ( int x, int y ) const;
    int                          searchMapBFS     ( vector<vector<int>> & beenThere, int r, int c) const;
    bool                         searchPath       ( vector<vector<int>> & beenThere, list<TCoord> & res,
                                                    int r, int c, int dst) const;
    CEntity *                    readSymbol       ( char symbol , int xPos);
    bool                         parseLine        ( const string & line );
    friend ostream &             operator <<      ( ostream & os, const CMap & map);
public:
            CMap            ( );
            CMap            ( const CMap & src ) = delete;
            ~CMap();
    void    operator =      ( const CMap & src ) = delete;
    bool    loadMap         ( const string & filename );
    bool    saveAttackers   ( ofstream & ofs );
    bool    saveTowers      ( ofstream & ofs );
    bool    findPaths       ( );
    bool    addAttacker     ( const CAttacker & attacker, int entranceNr );
    bool    addAttacker     ( const CAttacker & attacker, int entranceNr, int hp, int steps );
    bool    addTower        ( const CTower    & tower   , int row, int col );
    int     attackerCnt     ( ) const { return (int) m_attackers.size(); } ///Returns how many attackers are in map
    int     move            ( int maxSp );
    int     resetSpeed      ( );
    bool    displaceTowers  ( const vector<pair <CTower*, int>> &towers );
    void    attack          ( bool towers );
    int     width           ( ) const { return m_width;}
};


#endif //V1_1_CMAP_H
