#ifndef V1_1_TOWERS_H
#define V1_1_TOWERS_H
#include "entities.h"
using namespace std;


/** MEDIC
 *  20% chance of blocking attack coming from Warrior
 *  33% chance of surviving death and restoring 10 health
 *  20% chance of restoring health after being hit ( hp + damage / 2 )
 */
class CMedic : public CTower {
public:
    CMedic ( char symbol, const string & name, int hp, int attack, int range, int parentID )
        : CTower( symbol, name, hp, attack, range, parentID ),
          m_blockingChance(5),     //one in 5 = 20%
          m_resurrectionChance(3), // one in 3 = 33%
          m_restoreChance(5)       //one in 5 = 20%
    {}
    CTower    *  clone         ( ) const override { return new CMedic (*this); }     ///Clones this object
    void attack                ( CAttacker & dst ) override { dst.defend(*this);}    ///Attacks dst
    void         defend        ( CAttacker & src ) override;
    void         defend        ( CWarrior & src )  override;
    void         printDetails  ( ostream & os ) const override;
    void         print         ( ostream & os ) const override;
protected:
    int m_blockingChance;
    int m_resurrectionChance;
    int m_restoreChance;
};


/** HEAVY
 *  20% chance of blocking all attacks
 *  10% chance of damage to attacker when hit
 */

class CHeavy : public CTower {
public:
    CHeavy ( char symbol, const string & name, int hp, int attack, int range, int parentID )
            : CTower( symbol, name, hp, attack, range,  parentID ),
              m_blockingChance(5), //one in 5 = 20%
              m_attackChance(10) // one in 10 == 10%
    {}
    CTower    *  clone         ( ) const override { return new CHeavy (*this); }  ///Clones this object
    void         attack        ( CAttacker & dst ) override { dst.defend(*this);} ///Attacks dst
    void         defend        ( CAttacker & src ) override ;
    void         printDetails  ( ostream & os ) const override;
    void         print         ( ostream & os ) const override;
protected:
    int m_blockingChance;
    int m_attackChance;
};


/** ARTILLERY
 * 25% Chance of larger distance by half
 * 20% Chance of double shot
 */
class CArtillery : public CTower {
public:
    CArtillery ( char symbol, const string & name, int hp, int attack, int range, int parentID )
            : CTower( symbol, name, hp, attack, range, parentID ),
              m_extendedDistanceChance(4), //one in 4 = 25%
              m_doubleAttackChance(5)      // one in 5 = 20%
    {}
    CTower    *  clone         ( ) const override { return new CArtillery (*this); } ///Clones this object
    bool         hasInRange    (const TCoord &src) const override;
    void         attack        ( CAttacker & dst ) override;
    void         printDetails  ( ostream & os ) const override;
    void         print         ( ostream & os ) const override;
protected:
    int m_extendedDistanceChance;
    int m_doubleAttackChance;
};

#endif //V1_1_TOWERS_H
