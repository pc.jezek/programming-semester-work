#include <fstream>
#include "config.h"

CConfig CONFIG;

/** Loads config file
 * stores paths to important files and folders used
 * in the game
 * @param file to be loaded
 * @return success / not
 */
bool CConfig::load(const string & file) {
    m_config = file;
    ifstream ifs ( m_config );
    if ( ifs.fail() )
        return false;
    string tmpString;
    getline(ifs, tmpString);
    ifs >> tmpString; ifs >> m_mapsFolder;
    if ( tmpString != "MAPS_FOLDER") return false;
    ifs >> tmpString; ifs >> m_gamesFolder;
    if ( tmpString != "GAMES_FOLDER") return false;
    ifs >> tmpString; ifs >> m_scoreFile;
    if ( tmpString != "SCORE") return false;
    ifs >> tmpString; ifs >> m_menuFile;
    if ( tmpString != "MENU") return false;
    ifs >> tmpString; ifs >> m_color;
    if ( tmpString != "COLOR") return false;
    if ( ifs.fail() ) {
        ifs.close();
        return false;
    }
    ifs.close();
    return true;
}
