#include <iostream>
#include "menu.h" // also includes game.h, cmap.h, entities.h, coords.h, config.h
using namespace std;

/** \mainpage Basic info
 * \section intro Introduction
 * Tower attack is a simple game, where player can select different attackers
 * and place them into one of entrances to cope with towers which are displaced
 * all over the map.
 * \section menu Menu
 * To navigate through menu use numbers that are next to the items you want to select.
 * If you want to go back press q or follow the instructions on the screen.
 * \subsection new New game
 * In this section of the menu, you can choose between all kinds of different maps.
 * If you type in the number next to the map name, you start a new game with this map.
 * \subsection load Load game
 * In Load game submenu, you can load existing games you've saved previously.
 * Just type in the number next to the game you want to play.
 * \subsection score Scores
 * You can see 6 of your best runs there.
 * \subsection manual Manual
 * To navigate in this section, use N as next, P - previous or Q - quit.
 * You can see all kinds of different abilities of the attackers and towers
 * \section game Game
 * \subsection attackers Attackers
 * There are several different types of attackers:
 * \subsubsection warrior Warrior
 *       33% chance of blocking attack from artillery
 *       10% chance of attacking twice
 * \subsubsection mage Mage
 *       50% chance of attacking twice
 *       10% chance of blocking attack from medic
 *       10% chance of getting sick and dying when attacked
 * \subsubsection archer Archer
 *       50% chance of extending range by half
 *       10 % chance of attacking twice
 * \subsection towers Towers
 * There are several different types of towers, too:
 * \subsubsection medic Medic
 *       20% chance of blocking attack coming from Warrior
 *       33% chance of surviving death and restoring 10 health
 *       20% chance of restoring some health after being hit
 * \subsubsection heavy Heavy
 *     20% chance of blocking all attacks
 *     10% chance of damage to attacker when hit
 * \subsubsection artillery Artillery
 *     25% chance of extending range by half
 *     20% Chance of double shot
 * \section how How to play
 * To play the game, simply type attacker's symbol ( e.g @ ) followed by
 * entrance number where you want to place him,
 * so e.g. @ 1 puts attacker @ to 1st entrance.
 * Goal of the game is to get as many attackers to the end as possible.
 * If you just press enter without typing anything, the attackers move.
 */


///main, loads config and menu
int main ( ) {
    ///loads config file
    if ( ! CONFIG.load("./config") ){
        cout << "Can't load config." << endl;
    }

    ///Creates new menu.
    CMenuController menu ;
    ///Draw Main menu and select option
    try { menu.start(); }
    catch ( ... ){
        cout << "Couldn't load menu!" << endl;
    }
    return 0;
}