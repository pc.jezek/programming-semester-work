#ifndef V1_1_ENTITIES_H
#define V1_1_ENTITIES_H
#include <iostream>
#include <list>
#include "coords.h"
#include "config.h"

//front declarations
class CAttacker;
class CWarrior;
class CMedic;
class CArtillery;

using namespace std;

/// Abstract class, used to store entities to map
class CEntity {
public:
    ///Returns whether the field is discoverable ( if it's either free space or path )
    virtual bool discover      ( ) const {return false;};
    virtual void print         ( ostream & os ) const = 0;  ///Prints the object ( it's symbol )
    virtual bool isPath        ( ) const { return false;}   ///Returns whether the field is path or not
    virtual ~CEntity() = default;                           ///virtual destructor
};

/** Tower is one of the main game objects, it can attack and defend itself from other attackers,
 * it remembers it's position, can save itself to a file and clones itself
 */
class CTower : public CEntity{
public:
            CTower  ( char symbol, const string & name, int hp, int attack, int range, int parentID );
    virtual ~CTower() = default;                                           ///virtual destructor
    virtual void print         ( ostream & os )  const { os << m_symbol; } ///prints the symbol of the tower
    virtual void printDetails  ( ostream & os )  const;
    void         save          ( ofstream & ofs) const;
    void         setPos        ( TCoord pos ) { m_pos = pos;}       ///setter for position of the tower
    TCoord       position      ( ) const { return m_pos;}           ///getter for position of the tower
    int          getHp         ( ) const { return m_hp; }           ///getter for health
    int          setHp         (int hp) { m_hp = hp; return m_hp; } ///setter for health
    virtual void defend        ( CAttacker & src );
    virtual void defend        ( CWarrior & src );
    virtual void attack        ( CAttacker & dst );
    int          getAttack     () const { return m_attack;}         ///getter for attack strength
    virtual bool hasInRange    ( const TCoord & src ) const ;
    virtual CTower *  clone    ( ) const = 0;                       ///virtual clone method
    friend ostream & operator << ( ostream & os, const CTower & tower );
protected:
    TCoord  m_pos;
    char    m_symbol;
    string  m_name;
    int     m_hp;
    int     m_attack;
    int     m_range;
    int     m_parentID;
};

/** Attacker is one of the main game objects, it can attack and defend itself from other towers,
 * it remembers it's position, can save itself to a file, move itself and clones itself
 */
class CAttacker: public CEntity{
public:
            CAttacker  ( char symbol, const string & name, int hp,
                         int attack, int range, int speed, int parentID);
    virtual ~CAttacker() = default;                                         ///virtual destructor
    virtual void print         ( ostream & os ) const { os << m_symbol; }   ///prints attacker's symbol
    virtual void printDetails  ( ostream & os ) const;
    void         setPath       ( const list <TCoord> &path );
    void         setEntrance   (int entranceIdx) { m_entranceID = entranceIdx; } ///sets entrance ID
    bool         move          ( );
    void         save          ( ofstream & ofs ) const ;
    TCoord       nextMove      ( int maxSpeed );
    TCoord       position      ( ) const { return m_pos;}           ///setter for position of the attacker
    char         symbol        ( ) const { return m_symbol;}        ///getter for symbol of the attacker
    int          speed         ( ) const { return m_speed;}         ///getter for speed of the attacker
    void         resetSpeedCnt ( )       { m_speedCnt = 0;}         ///resets speed counter to 0
    virtual bool hasInRange    ( const TCoord & src ) const;
    virtual void defend        ( CTower & src );
    virtual void defend        ( CMedic & src );
    virtual void attack        ( CTower & dst );
    virtual void defend        ( CArtillery & src );
    int          getAttack     () const { return m_attack;}         ///getter for attack strength
    int          getHp         () const { return m_hp; }            ///getter for health
    int          setHp         (int hp) { m_hp = hp; return m_hp; } ///setter for health
    virtual CAttacker *  clone ( ) const = 0;                       ///virtual clone method
    friend ostream &operator<<(ostream &os, const CAttacker &attacker);
protected:
    TCoord                       m_pos;
    list<TCoord>                 m_pathToEnd;
    list<TCoord>::const_iterator m_pathIt;
    char    m_symbol;
    string  m_name;
    int     m_hp;
    int     m_attack;
    int     m_range;
    int     m_speed;
    int     m_speedCnt;
    int     m_parentID;
    int     m_steps;
    int     m_entranceID;
};

/** represents the wall ( barricade ) */
class CWall : public CEntity{
public:
        CWall ( char x ) : m_symbol (x){}
    virtual void print         ( ostream & os ) const;
protected:
    char m_symbol;
};

/** represents the path where attackers are moving */
class CPath : public CEntity{
public:
    CPath ( char x ) : m_symbol (x){}
    virtual bool discover      ( ) const override { return true; }
    virtual bool isPath        ( ) const override { return true;}
    virtual void print         ( ostream & os ) const override { os << m_symbol; } ///prints the path
protected:
    char m_symbol;
};

/** represents free space - place, where tower can be inserted,
 * because attackers don't move through there */
class CFreeSpace : public CEntity{
public:
    CFreeSpace ( char x ) : m_symbol (x){}
    virtual bool discover      ( )              const override { return true; }
    ///prints the unoccupied field
    virtual void print         ( ostream & os ) const override { os << m_symbol; }
protected:
    char m_symbol;
};

/** represents entrance to the map, positions, where attackers can be inserted */
class CEntrance : public CEntity{
public:
    CEntrance ( char x, int row, int col ) : m_symbol (x), m_row (row), m_col (col){}
    virtual void print         ( ostream & os ) const           { os << m_symbol; }
            void setPath       ( const list < TCoord > & path ) { m_pathToEnd = path; }
    ///returns list with set of coordinates how to get to the end
    const list < TCoord > & getPath  ( ) const { return m_pathToEnd; }
            int             row      ( ) const { return m_row;} ///returns row coord
            int             col      ( ) const { return m_col;} ///returns col coord
protected:
    char m_symbol;
    int  m_row;
    int  m_col;
    list < TCoord > m_pathToEnd;
};

/** represents exit, place where attackers are heading */
class CExit : public CEntity {
public:
    CExit( char x, int row, int col ) : m_symbol (x), m_pos(row,col){}
    virtual bool discover      ( )  const override { return true;}      ///Exit is discoverable
    virtual void print(ostream &os) const override { os << m_symbol; }  ///prints the exit
    TCoord       position      ( )  const { return m_pos;}              ///returns position
            int  row           ( )  const { return m_pos.t_row;}        ///returns row coord
            int  col           ( )  const { return m_pos.t_col;}        ///returns col coord
protected:
    char m_symbol;
    TCoord m_pos;
};


#endif //V1_1_ENTITIES_H
