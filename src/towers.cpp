#include <iomanip>
#include "attackers.h" //also include towers.h, entities.h, coords.h, config.h


//-------------------------------- MEDIC ---------------------------------//
/** Medic's defend against warrior, using double dispatch technique,
 * if blocking chance is reached, the attack is blocked
 * @param war warrior attacking
 */
void CMedic::defend( CWarrior & war) {
    if ( calculateChance(m_blockingChance))
        return;
    CAttacker & tmp = war;
    CTower::defend(tmp);
}

/** Medic's defend against all attackers except warrior
 * if hp is below zero after attack, there is small chance,
 * that 10 health will be restored
 * sometimes, medic can restore some health after hit
 * @param src attacker attacking
 */
void CMedic::defend(CAttacker &src) {
    CTower::defend(src);
    if ( m_hp <= 0) {
       if ( calculateChance(m_resurrectionChance))
            m_hp = 10;
        return;
    }
    if ( calculateChance(m_restoreChance))
        m_hp += src.getAttack()/2;
}

/** Prints medic's details */
void CMedic::printDetails(ostream &os) const {
    if (CONFIG.m_color) os <<"\033[1;" << MEDIC_COLOR << "m";
    os << setw(10) << "MEDIC ";
    if (CONFIG.m_color) os << "\033[0m";
    CTower::printDetails(os);
}

/** Prints medic's symbol */
void CMedic::print(ostream &os) const {
    if (CONFIG.m_color) os <<"\033[1;" << MEDIC_COLOR << "m";
    CTower::print(os);
    if (CONFIG.m_color) os << "\033[0m";
}

//-------------------------------- HEAVY ---------------------------------//
/** Heavy's defend against all attackers
 * sometimes, attack can be blocked and sometimes
 * heavy can hit back
 * @param src
 */
void CHeavy::defend(CAttacker &src) {
    if ( calculateChance(m_blockingChance))
        return;
    CTower::defend(src);
    if ( calculateChance(m_attackChance))
        src.setHp( src.getHp() - m_attack/2);
}

/** Prints heavy's details */
void CHeavy::printDetails(ostream &os) const {
    if (CONFIG.m_color) os <<"\033[1;" << HEAVY_COLOR -10 << "m";
    os << setw(10) << "HEAVY ";
    if (CONFIG.m_color) os << "\033[0m";
    CTower::printDetails(os);
}

/** Prints heavy's symbol */
void CHeavy::print(ostream &os) const {
    if (CONFIG.m_color) os <<"\033[1;" << HEAVY_COLOR << "m";
    CTower::print(os);
    if (CONFIG.m_color) os << "\033[0m";
}

//-------------------------------- ARTILLERY -----------------------------//
/** Artillery's special ability,
 * it can sometimes see longer distance
 * @param src coordinates to compare distance with
 * @return if the coordinates are in artillery's reach
 */
bool CArtillery::hasInRange(const TCoord &src) const {
    return  abs( src.t_col - m_pos.t_col) <=
                    ( m_range  * (calculateChance(m_extendedDistanceChance) ? 1.5 : 1.0))
            && abs( src.t_row - m_pos.t_row) <=
                    ( m_range  * (calculateChance(m_extendedDistanceChance) ? 1.5 : 1.0)) ;
}

/** Artillery special ability,
 * it can sometimes attack twice
 * @param dst attacker to attack
 */
void CArtillery::attack(CAttacker &dst) {
    dst.defend(*this);
    if ( calculateChance(m_doubleAttackChance))
        dst.defend(*this);
}


/** Prints artillery's details */
void CArtillery::printDetails(ostream &os) const {
    if (CONFIG.m_color)  os <<"\033[1;" << ARTILLERY_COLOR << "m";
    os << setw(10) << "ARTILLERY ";
    if (CONFIG.m_color) os << "\033[0m";
    CTower::printDetails(os);
}

/** Prints artillery's details */
void CArtillery::print(ostream &os) const {
    if (CONFIG.m_color) os <<"\033[1;" << ARTILLERY_COLOR << "m";
    CTower::print(os);
    if (CONFIG.m_color) os << "\033[0m";
}