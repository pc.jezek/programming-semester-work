#include <iomanip>
#include "entities.h"
#include "towers.h"
#include "attackers.h"
#include <fstream>


/** Prints tower details
 * @param tower to print
 */
ostream &operator<<(ostream &os, const CTower &tower) {
    tower.printDetails(os);
    return os;
}

/** Prints attacker details
 * @param attacker to print
 */
ostream &operator<<(ostream &os, const CAttacker &attacker) {
    attacker.printDetails(os);
    return os;
}

//-------------------------------- ATTACKER ---------------------------------------//
/** Attacker constructor, sets member variables
 * @param symbol
 * @param name
 * @param hp health
 * @param attack how much damage can attacker cause
 * @param range how far does he see
 * @param speed how fast does he move
 * @param parentID who is his template ( parent )
 */
CAttacker::CAttacker(char symbol, const string &name, int hp,
                     int attack, int range, int speed, int parentID)
        : m_symbol ( symbol ), m_name ( name ), m_hp ( hp ),
          m_attack ( attack ), m_range ( range ), m_speed ( speed ),
          m_speedCnt(0), m_parentID (parentID), m_steps(0){}

/** Sets path to attacker.
 * When he enters the map, he needs to know how to get to the end,
 * so list of coordinates from the entrance he was inserted into
 * is copied into his member value
 * @param path to assign
 */
void CAttacker::setPath ( const list <TCoord> &path) {
    m_pathToEnd = path;
    m_pathIt = m_pathToEnd.begin();
    m_pathIt++;
    m_pos.t_row = m_pathIt->t_row;
    m_pos.t_col = m_pathIt->t_col;
    m_pathIt++;
}

/** Moves the attacker one step towards the exit
 * @return bool value if the attacker reached the end
 */
bool CAttacker::move() {
    m_pos.t_row = m_pathIt->t_row;
    m_pos.t_col = m_pathIt->t_col;
    m_pathIt++;
    m_steps++;
    return m_pathIt != m_pathToEnd.end();
}

/** Returns whether the given coordinates are in attacker's range
 * @param src coordinates to compare
 * @return if the src coordinates are in range
 */
bool CAttacker::hasInRange(const TCoord & src) const{
    return  abs( src.t_col - m_pos.t_col) <= m_range
            && abs( src.t_row - m_pos.t_row) <= m_range;
}

/** Returns the next position where attacker is heading,
 * if it has moved m_speed times and the map speed is faster,
 * it doesn't move and returns current position
 * @param maxSpeed what is the current maximum speed
 * @return next position of the attacker
 */
TCoord CAttacker::nextMove(int maxSpeed) {
    m_speedCnt %= maxSpeed;
    if ( m_speedCnt < m_speed ){
        m_speedCnt++;
        return *m_pathIt;
    }
    m_speedCnt++;
    return m_pos;
}

/** Default defend method of the attackers
 * @param src Tower that attacks this attacker
 */
void CAttacker::defend(CTower &src) {
    m_hp -= src.getAttack();
}

/** Default defend method against medic */
void CAttacker::defend(CMedic &src) {
    CTower & tmp = src;
    defend(tmp);
}

/** Default defend method against artillery */
void CAttacker::defend(CArtillery &src) {
    CTower & tmp = src;
    defend(tmp);
}

/** Prints attacker details to stream */
void CAttacker::printDetails(ostream &os) const {
    print(os); os<< " ";
    os << setw(15) << left << m_name;
    os << setw(5) << m_hp;
    os << setw(8) << m_attack;
    os << setw(7) << m_range;
    os << setw(7) << m_speed;
}

/** Default attack method of the attacker
 * @param dst Attacker to attack
 */
void CAttacker::attack ( CTower &dst) {
    dst.defend(*this);
}

/** Save attacker to stream given by parameter
 * @param ofs stream to save attacker into
 */
void CAttacker::save(ofstream &ofs) const {
    ofs << 'A' << "," << m_parentID << "," << m_hp << "," << m_steps << "," << m_entranceID << endl;
}

//-------------------------------- TOWER ---------------------------------------//
/** Tower constructor, sets member variables
 * @param symbol
 * @param name
 * @param hp health
 * @param how much damage can tower cause
 * @param range how far does it see
 * @param parentID who is it's template ( parent )
 */
CTower::CTower(char symbol, const string &name, int hp, int attack, int range, int parentID)
        : m_symbol ( symbol ), m_name ( name ), m_hp ( hp ), m_attack ( attack ),
          m_range ( range ), m_parentID ( parentID )  {}

/** Returns whether the given coordinates are in tower's range
 * @param src coordinates to compare
 * @return if the src coordinates are in range
 */
bool CTower::hasInRange(const TCoord &src) const{
    return  abs( src.t_col - m_pos.t_col)    <= m_range
            && abs( src.t_row - m_pos.t_row) <= m_range;
}

/** Default defend method of the tower
 * @param src Attacker that attacks this tower
 */
void CTower::defend( CAttacker & src ) {
    m_hp -= src.getAttack();
}

/** Default attack method of the tower
 * @param dst Attacker to attack
 */
void CTower::attack (CAttacker &dst ) {
    dst.defend(*this);
}

/** Default defend method against warrior */
void CTower::defend(CWarrior &src) {
    CAttacker & tmp = src;
    defend(tmp);
}

/** Save tower to stream given by parameter
 * @param ofs stream to save tower into
 */
void CTower::save(ofstream & ofs ) const {
    ofs << 'T' << "," << m_parentID << "," << m_hp << "," << m_pos.t_row<< "," << m_pos.t_col << endl;
}

/** Prints tower details to stream */
void CTower::printDetails(ostream &os) const {
    print(os); os<< " ";
    os << setw(15) << left << m_name;
    os << setw(5) << m_hp;
    os << setw(8) << m_attack;
    os << setw(7) << m_range;
}

//-------------------------------- WALL ---------------------------------------//
/** Prints wall, with or without color prefix based on config file */
void CWall::print(ostream &os) const {
    if ( CONFIG.m_color )
        os <<"\033[45m";
    os << (CONFIG.m_color ? ' ' : m_symbol);
    if ( CONFIG.m_color)
        os << "\033[0m";
}