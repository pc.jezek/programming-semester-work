#ifndef V1_1_ATTACKERS_H
#define V1_1_ATTACKERS_H
#include <vector>
#include "entities.h"
#include "towers.h"

using namespace std;

bool calculateChance (int chance);

/** Warrior
 *  33% chance of blocking attack from artillery,
 *  10 % chance of attacking twice
 */
class CWarrior : public CAttacker {
public:
    CWarrior (char symbol, const string &name, int hp, int attack, int range, int speed, int parentID)
    : CAttacker ( symbol, name, hp, attack, range, speed, parentID),
      m_blockingChance(3), // one in 3 = 33%
      m_twiceChance(10) // one in 10 = 10%
     {}
    CAttacker *  clone         ( ) const override { return new CWarrior (*this); } ///clones this object
    void         attack        ( CTower & dst )     override;
    void         defend        ( CArtillery & src ) override;
    void         printDetails  ( ostream & os ) const override;
    void         print         ( ostream & os ) const override;


protected:
    int m_blockingChance;
    int m_twiceChance;
};

/** Archer
 *  50% chance of extending range by half,
 *  10 % chance of attacking twice
 */
class CArcher : public CAttacker {
public:
    CArcher (char symbol, const string &name, int hp, int attack, int range, int speed, int parentID)
            : CAttacker ( symbol, name, hp, attack, range, speed, parentID),
              m_extendedDistanceChance(2), // one in 2 = 50%
              m_twiceChance(10) // one in 10 = 10%
    {}
    CAttacker *  clone         ( ) const override { return new CArcher (*this); } ///clones this object
    bool         hasInRange    ( const TCoord & src ) const override;
    void         attack        ( CTower & dst ) override;
    void         printDetails  ( ostream & os ) const override;
    void         print         ( ostream & os ) const override;


protected:
    int m_extendedDistanceChance;
    int m_twiceChance;
};

/** Mage
 *  50% chance of attacking twice,
 *  10% chance of blocking attack from medic,
 *  10% chance of getting sick and dying when attacked
 */
class CMage : public CAttacker {
public:
    CMage (char symbol, const string &name, int hp, int attack, int range, int speed, int parentID)
            : CAttacker ( symbol, name, hp, attack, range, speed, parentID),
              m_blockingChance(1), // one in 10 = 10%
              m_twiceChance(2),     // one in 2 = 50%
              m_dyingChance(1)
    {}
    CAttacker *  clone         ( ) const override { return new CMage (*this); } ///clones this object
    void         attack        ( CTower & dst ) override;
    void         defend        ( CTower & src ) override;
    void         defend        ( CMedic & src ) override;
    void         printDetails  ( ostream & os ) const override;
    void         print         ( ostream & os ) const override;

protected:
    int m_blockingChance;
    int m_twiceChance;
    int m_dyingChance;
};

#endif //V1_1_ATTACKERS_H
