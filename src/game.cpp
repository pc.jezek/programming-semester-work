#include <fstream>
#include <sstream>
#include <iomanip>
#include <climits>
#include <algorithm>
#include "game.h"      // also includes cmap.h, entities.h, coords.h, config.h
#include "attackers.h" // also includes entities.h, coords.h, config.h, towers.h

using namespace std;

/**Prepares game, loads new or saved game
 * @param filename Map / Saved game, depends on newGame
 * @param newGame Determines whether new game is created or saved game is loaded
 * @return success / corrupted file
 */
bool CGame::loadGame(const string &filename, bool newgame) {
    m_mapFolder = filename;
    if ( newgame ) {
        return m_gamePlan.loadMap(filename + "/map")
               && loadEntities(filename + "/setup")
               && m_gamePlan.displaceTowers(m_towers);
    }
    else {
        return   loadSavedGame( filename );
    }
}

/**
 * Loads entities (CTower, CAttacker) from settings file and stores them
 * @param filename settings file to load
 * @return  success / corrupted file
 */
bool CGame::loadEntities(const string &filename) {
    ifstream ifs ( filename );
    if ( ifs.fail() )
        return false;
    string line;
    while ( true ) {
        getline ( ifs, line, '\n' );
        if ( ifs. eof () || ! line.size() ) break;
        if ( line [0] == '#' || line[0] == '\n') continue;
        if ( ! parseLine ( line ) || ifs.fail() ) {
            ifs.close();
            return false;
        }
    }
    for (const auto & attacker : m_attackers)
        m_attackersTotal += attacker.second;
    ifs.close();
    return true;
}

/** Parses one line from inputFile / inputString,
 * loads appropriate attackers / towers to m_attackers / m_towers
 * @param line string to parse
 * @return success / corrupted data
 */
bool CGame::parseLine(const string &line) {
    istringstream is ( line );
    char    comma1,comma2,comma3,comma4,comma5, comma6, lBracket, rBracket;
    char    symbol, type;
    string  OBJECT, name;
    int     hp, quantity, attack, range, speed;
    is >> skipws    >> OBJECT >> lBracket >> type >> comma1 >> symbol >> comma2
       >> quantity  >> comma3 >> name;
    if ( name.size() && name [ name.size() -1 ] == ',' ){
          name.erase(name.end()-1); comma4 = ',';
    } else is >> comma4;
    is >>  hp
       >> comma5    >> attack >> comma6      >> range;
    if (    comma1 != ',' || comma2 != ',' || comma3   != ','
         || comma3 != ',' || comma4 != ',' || comma5   != ','
         || comma6 != ',' || lBracket != '{'
         || hp < 1 || quantity < 1 || attack < 0 || range < 1
         || is.fail() )
        return false;
    if ( OBJECT == "TOWER" ) {
        is >> rBracket;
        if ( rBracket != '}' || is.fail() )
            return false;
        CTower * newTower;
        switch ( type ){
            case 'M':
                newTower = new CMedic(symbol, name, hp, attack, range, (int)m_towers.size()); break;
            case 'A':
                newTower = new CArtillery(symbol, name, hp, attack, range, (int)m_towers.size()); break;
            case 'H':
                newTower = new CHeavy(symbol, name, hp, attack, range, (int)m_towers.size()); break;
            default: return false;
        }
        m_towers.push_back(make_pair( newTower, quantity ));
    } else if ( OBJECT == "ATTACKER") {
        is >> comma1 >> speed >> rBracket;
        if (comma1 != ',' || rBracket != '}' || is.fail() ) return false;
        CAttacker * newAttacker;
        switch ( type ){
            case 'W':
                newAttacker = new CWarrior(symbol, name, hp, attack, range, speed,  (int)m_attackers.size()); break;
            case 'A':
                newAttacker = new CArcher(symbol, name, hp, attack, range, speed , (int)m_attackers.size()); break;
            case 'M':
                newAttacker = new CMage(symbol, name, hp, attack, range, speed,  (int)m_attackers.size()); break;
            default: return false;
        }
        m_attackers.push_back(make_pair( newAttacker, quantity ));
    } else return false;
    return true;
}

/** Output stream operator,
 * this prints the whole game
 * @param game to print
 */
ostream &operator<<(ostream &os, const CGame &game){
    system("clear");
    string score ("SCORE:");
    os << score<< setw(game.m_gamePlan.width() - (int)score.length()) << right << game.m_score << endl;
    os << left << game.m_gamePlan;
    os << "------------------------- Towers -------------------------" << endl;
    os << setw(10) << "Type" << "  " << setw(15) << "Name" << setw(5) << "Hp"
       << setw(8) <<  "Attack" << setw(7) << "Range" << setw(7) << "Speed"  << setw(8) << "Number" << endl;
    for ( const auto & tower : game.m_towers)
        os << *tower.first << setw(7) << "" <<  "x " << tower.second << endl;
    os << "------------------------ Attackers -----------------------" << endl;

    for ( const auto & attacker : game.m_attackers)
        os << *attacker.first << "x " << attacker.second << endl;

    os << endl << "Q - QUIT\t\tS - SAVE" << endl;
    return os;
}

/** Moves all attackers respectively to their speed and animates
 * one moving session
 */
void CGame::moveAndPrint() {
    int maxSp = m_gamePlan.resetSpeed();
    if (! maxSp)
        return;
    for ( int i = 0; i < maxSp ; ++i) {
        system ( "sleep 0.05");
        cout << *this << endl;
        m_score += (m_gamePlan.move(maxSp) * ( MAX_SCORE / m_attackersTotal)  );
    }
    system ( "sleep 0.05");
    cout << *this << endl;
}


/** After the game is loaded, this method
 * is the main game controller, it calls attacks, moves the board and
 * maintains the user input
 * @return whether the player won or not ( more than half the attackers made it to the end )
 */
bool CGame::play(void) {
    while (true){
        cout << *this << endl;
        while (true) {
            bool res;
            try { res = readNextMove(); }
            catch ( ... ){ throw; }
            if ( res )
                break;
            cin.clear();
            cout << *this;
            cout << "Wrong move!" << endl;
        }
        cout << *this << endl;
        m_gamePlan.attack ( true );
        m_gamePlan.attack ( false );
        moveAndPrint();
        int count = 0;
        for ( const auto & x: m_attackers)
            if ( ( count += x.second )  )
                break;
        count += m_gamePlan.attackerCnt();
        if ( ! count ) {
            saveScore();
            return m_score > MAX_SCORE / 2;
        }
    }
}

/** Handles user input, places Attackers into the map
 * or saves the game when S is pressed
 * @return if the input was correct or not
 */
bool CGame::readNextMove() {
    cout << "NEXT MOVE: ";
    string line;
    getline(cin, line);
    if ( cin.fail() )
        return false;
    if (line.empty())
        return true;
    istringstream is ( line );
    int x = -1; char symbol;
    is >> symbol ;
    char lower_symbol = (char) tolower(symbol);
    if ( is.fail() )
       return false;
    if ( lower_symbol == 'q')
        throw quit();
    else if ( lower_symbol == 's') {
        cout << "FILENAME: ";
        string filename;
        getline ( cin, filename);
        saveGame(filename);
        throw quit();
    }
    is >> x;
    if ( is.fail() || x <= 0)
        return false;
    for ( auto & attacker : m_attackers){
        if ( attacker.first -> symbol () == symbol ) {
            if (!attacker.second || ! m_gamePlan.addAttacker( *attacker.first ,x) )
                return false;
            attacker.second--;
            return true;
        }
    }
    return false;
}

/** Loads saved game from file given by parameter
 * @param filename to load
 * @return whether the game was loaded successfully
 */
bool CGame::loadSavedGame(const string &filename) {
    ifstream ifs ( filename );
    string mapFolder;
    getline (ifs, mapFolder);
    m_mapFolder = mapFolder;
    ifs >> m_score;
    ifs.ignore(INT_MAX, '\n');
    if ( ! m_gamePlan.loadMap(mapFolder + "/map")
         || ! loadEntities( mapFolder + "/setup") ) {
        ifs.close();
        return false;
    }
    for ( auto & attacker : m_attackers)
        ifs >> attacker.second;
    ifs.ignore(INT_MAX, '\n');
    string line;
    while ( true ) {
        getline ( ifs, line, '\n' );
        if ( ifs. eof () || ! line.size() ) break;
        if ( line [0] == '#' || line[0] == '\n') continue;
        if ( ! parseSave ( line ) || ifs.fail() ) {
            ifs.close();
            return false;
        }
    }
    ifs.close();
    return true;
}

/** Parses savefile and saves appropriate attackers / towers
 * @param line to parse
 * @return if the input is correct
 */
bool CGame::parseSave(const string &line) {
    istringstream is ( line );
    char type, dummy;
    int  parent,  hp, steps, entrance, row, col;
    is >> type >> dummy >> parent >> dummy;
    switch ( type ) {
        case 'A':{
            is >> hp >> dummy >> steps >> dummy >> entrance;
            if ( ! m_gamePlan.addAttacker(*m_attackers[parent].first, entrance, hp, steps) )
                return false;
            break;
        }
        case 'T': {
            is >> hp >> dummy >> row >> dummy >> col;
            CTower *newTower = m_towers[parent].first->clone();
            newTower->setHp(hp);
            m_gamePlan.addTower(*newTower, row, col);
            delete newTower;
            break;
        }
        default: return false;
    }
    return ! is.fail();
}

/** Saves a game to a file given by parameter
 * structure of a savefile is:
 * Map folder
 * Score
 * how many instances of each attacker are left
 * attackers in the map  - A, parent ( template ) id, hp, steps, entrance
 * towers in the map - T, parent ( template ) id, hp, row, col
 * @param outfile where to save the game
 * @return success / not
 */
bool CGame::saveGame(const string &outfile) {
    string saveFile = CONFIG.m_gamesFolder + "/" + outfile;
    ofstream ofs ( saveFile );
    ofs << m_mapFolder << endl;
    ofs << m_score << endl;
    for ( const auto & attacker : m_attackers)
        ofs << attacker.second << endl;
    m_gamePlan.saveAttackers(ofs);
    m_gamePlan.saveTowers(ofs);
    ofs.close();
    return ofs.fail();
}

/** Saves score to file which was set in Config file */
void CGame::saveScore() const {
    ifstream ifs ( CONFIG.m_scoreFile);
    if ( ifs.fail() )
        return;
    vector < int > scores;
    int score;
    while ( true ) {
        ifs >> score;
        if ( ifs.fail() )
            break;
        scores.push_back(score);
    }
    scores.push_back(m_score);
    sort ( scores.begin(), scores.end(), [] (int a, int b){ return a > b;});
    ifs.close();
    ofstream ofs (CONFIG.m_scoreFile);
    if ( ofs.fail() )
        return;
    for ( size_t i = 0; i < scores.size(); ++i ) {
        if ( i >= 6 )
            break;
        ofs  << scores[i] << endl;
    }
    ofs.close();
}

///Destructor, deletes parent towers / attackers pointers
CGame::~CGame() {
    for ( const auto & attackerPtr: m_attackers)
        delete attackerPtr.first;
    for ( const auto & towerPtr: m_towers)
        delete towerPtr.first;
}