#ifndef V1_1_GAME_H
#define V1_1_GAME_H

#include "cmap.h"   //also includes entities.h, coords.h, config.h
#include <vector>
#define MAX_SCORE 10000

///used for exception only
class quit{};

/**
 * CGame represents the game with its engine, gamePlan, entities, etc...
 * Is responsible for loading, saving and the actual gamePlay
 */
class CGame {
protected:
    int                                  m_score;
    int                                  m_attackersTotal;
    string                               m_mapFolder;
    CMap                                 m_gamePlan;
    vector < pair <CTower *,    int > >  m_towers;      ///All loaded tower templates ( parents )
    vector < pair <CAttacker *, int > >  m_attackers;   ///All loaded attacker templates ( parents )
    bool    parseLine    ( const string & line );
    bool    parseSave    ( const string & line );
    bool    loadEntities ( const string & filename );
    bool    readNextMove ( void );
    void    moveAndPrint ( void );
    bool    loadSavedGame( const string & filename);
    void    saveScore    () const;
    friend  ostream & operator<< (ostream &os, const CGame &game);
public:
            CGame       ( void ): m_score(0), m_attackersTotal (0) {}
            CGame       ( const CGame & src ) = delete;
            ~CGame();
    void    operator =  ( const CGame & src ) = delete;
    bool    loadGame    ( const string & filename, bool newGame );
    bool    saveGame    ( const string & outfile );
    bool    play        ( void );
};

#endif //V1_1_GAME_H