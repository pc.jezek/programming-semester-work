#ifndef V1_1_CONFIG_H
#define V1_1_CONFIG_H
#include <string>
using namespace std;

///color codes of some entities
#define WARRIOR_COLOR   33
#define ARCHER_COLOR    34
#define MAGE_COLOR      36
#define MEDIC_COLOR     32
#define HEAVY_COLOR     41
#define ARTILLERY_COLOR 31




/** Config, which needs to be loaded for the game to work
 * use load method to load config from file
 */
struct CConfig {
    CConfig ( ){}
    bool    load ( const string & file);
    string  m_config;
    string  m_mapsFolder;
    string  m_gamesFolder;
    string  m_scoreFile;
    string  m_menuFile;
    bool    m_color;
};

///global config object, stores all important paths
extern CConfig CONFIG;

#endif //V1_1_CONFIG_H
