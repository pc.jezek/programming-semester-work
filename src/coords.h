#ifndef V1_1_GLOBAL_H
#define V1_1_GLOBAL_H

/** Coordinates, row and col */
struct TCoord {
    int t_row, t_col;
    TCoord(int x,int y):t_row(x),t_col(y){};
    TCoord():t_row(-1),t_col(-1){};
    bool operator == ( const TCoord & b) const { return t_row == b.t_row && t_col == b.t_col;}
};

#endif //V1_1_GLOBAL_H
