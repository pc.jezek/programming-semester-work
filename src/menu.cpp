#include <fstream>
#include "menu.h" // also includes game.h, cmap.h, entities.h, coords.h, config.h
#include <dirent.h>
#include <algorithm>

/** Reads input from user, if read fails, exception is thrown
 * @return input that was read
 */
string CMenu::readInput ( ) const {
    cout << "YOUR OPTION: " << flush;
    string sel;
    getline( cin, sel );
    if ( cin.fail() || sel.empty())
        throw "Wrong option";
    return sel;
}

/** Prints x empty lines with borders,
 * size of each line is DEFAULTWIDTH
 * @param lines number of empty lines to print
 */
void CMenu::printEmptyLine(int lines ) const {
    for ( int i = 0; i < lines; ++i) {
        cout << "*";
        for (int j = 0; j < DEFAULTWIDTH - 2; ++j)
            cout << " ";
        cout << "*" << endl;
    }
}

/** Prints string to the position given by parameter,
 * string is surrounded by menu border, e.g. *    string     *
 * @param str string to print
 * @param position LEFT (0), CENTER(1) or RIGHT(2) available
 */
void CMenu::printString(const string &str, int position) const {
    double pos = CENTER;
    switch (position){
        case LEFT: pos = 5; break;
        case CENTER: pos = 2; break;
        case RIGHT: pos = 1; break;
        default: break;
    }
    cout << "*";
    int spaceLength = (int) ((DEFAULTWIDTH - (int) str.length() - 2)/pos);
    if ( position == RIGHT)
        spaceLength -= 10;
    for ( int j = 0; j < spaceLength ; ++j) {
        cout << " ";
    }
    cout << str;
    for ( int j = 0; j < DEFAULTWIDTH - (int)str.length() - spaceLength - 2; ++j)
        cout << " ";
    cout << "*";
    cout << endl;
}

/** Prints whole screen of DEFAULTHEIGHT x DEFAULTWIDTH size,
 * with message in the middle and quit info at the end
 * @param line message to print
 */
void CMenu::middleLine(const string &line) const {
    system("clear");
    printBorder();
    for (int i = 0; i < DEFAULTHEIGHT - 2; ++i) {
        if ( i == ( DEFAULTHEIGHT - 2) / 2) {
            printString(line);
            continue;
        }
        if ( i == DEFAULTHEIGHT - 4){
            printString("PRESS Q TO GO BACK");
            continue;
        }
        printEmptyLine();
    }
    printBorder();
}

/** Prints whole screen of DEFAULTHEIGHT x DEFAULTWIDTH size,
 * with error message in the middle
 * @param error message to print
 */
void CMenu::printError(const string & error) const {
    middleLine(error);
}

/** Loads folder, returns vector of filenames
 * @param folder to load
 * @return vector of filenames inside the folder, empty if error occurred
 */
vector<string> CMenu::loadFolder(const string &folder) const {
    DIR *dir; struct dirent * file;
    dir = opendir( folder.c_str() );
    if ( ! dir ){
        closedir(dir);
        return vector<string> ();
    }
    vector<string> files;
    while ( (file = readdir(dir) ) ) {
        if (file->d_name[0] != '.') {
            files.push_back( string ( file->d_name) );
        }
    }
    closedir(dir);
    return files;
}

/** Prints content of folder given by parameter,
 * each file / subfolder is separated by empty line
 * left and right boders are printed along both sides
 * @param folder
 * @return number of files inside folder
 * @throw exception thrown if folder is empty or if it was not loaded properly
 */
int CMenu::printFolder(const string &folder) const {
    vector <string> files = loadFolder(folder);
    if ( files.empty() )
        throw "Empty folder";

    sort ( files.begin(), files.end());
    int cnt = 0;
    for ( const auto & x : files){
       cnt++;
        printString(to_string(cnt) + ".   " + x);
           printEmptyLine();
    }
    return (int)files.size();
}

/// Prints border consisting of symbol '*' DEFAULTWIDTH symbols long
void CMenu::printBorder() const {
    for ( int i = 0; i < DEFAULTWIDTH ; ++i)
        cout << "*";
    cout << endl;
}

/** Prints menu screen with folder content given by parameter
 * @param folder to print
 */
void CMenu::printFolderMenu(const string &folder) const {
    int fileCnt;
    printBorder();
    printEmptyLine();
    printString("SELECT GAME:");
    printString("----------------------");
    printEmptyLine();
    try { fileCnt = printFolder ( folder ); }
    catch ( ... ) { printError("OOPS... NO FILES FOUND!"); return;}
    int end = DEFAULTHEIGHT - fileCnt*2 - 6 - 2;
    for ( int i = 0; i < end; ++i) {
        printEmptyLine();
    }
    printString("PRESS Q TO GO BACK");
    printEmptyLine();
    printBorder();
}

/** Processes input from user,
 * default menu selection tool, only quit to main menu is available
 * @throw exception thrown if the input is incorrect
 * @return pointer to main menu
 */
CMenu *CMenu::selectOption() {
    char option;
    string sel = readInput();
    option = (char) tolower (sel[0]);

    switch ( option ){
        case 'q':
            return new CMainMenu ;
        default:
            throw "Wrong option";
    }
}

/** Starts loaded game
 * @param game to play
 * @return pointer to menu screen that follows ( loser / winner )
 */
CMenu *CMenu::playGame(CGame &game) {
    bool result;
    try { result = game.play(); }
    catch ( const quit & q ){
        return new CMainMenu;
    }
    if ( result )
        return new CWinner;
    else
        return new CLoser;
}

/** Print main menu,
 * main menu is loaded from file ( MENU_FILE in CONFIG )
 * @throw exception thrown if menu file is corrupted or does not exist
 */
void CMainMenu::show() const {
    ifstream ifs ( CONFIG.m_menuFile );
    if (ifs.fail())
        throw CantOpenFile();
    while ( true ){
        char ch = (char)ifs.get();
        if ( ifs.fail() )
            break;
        cout << ch;
    }
    ifs.close();
}

/** Processes input from user,
 * user can select NEWGAME, LOADGAME, SCORES, MANUAL or QUIT
 * @throw exception thrown if the input is incorrect
 * @return pointer to the appropriate menu based on user's choice
 */
CMenu * CMainMenu::selectOption() {
    enum options { NEWGAME = 1,LOADGAME = 2, SCORES = 3, MANUAL = 4, QUIT = 5};
    string sel = readInput();
    int option = sel[0] - '0';
    switch ( option ){
        case NEWGAME:
            return new CNewMenu;
        case LOADGAME:
            return new CGamesMenu;
        case SCORES:
            return new CScoreMenu;
        case MANUAL:
            return new CManualPage1;
        case QUIT:
            system ("clear");
            system ("clear");
            exit (0);
        default: throw "Wrong selection";
    }
}

///Prints new game menu
void CNewMenu::show() const {
    printFolderMenu(CONFIG.m_mapsFolder);
}

/** Option selector for new game menu,
 * user can type the number of map to load, then game is loaded and started
 * if the map fails to load, error screen is printed
 * @return pointer to appropriate menu ( Main / New / Loser / Winner menu )
 * @throw exception thrown if the input is incorrect
 */
CMenu *CNewMenu::selectOption() {
    string sel = readInput();
    if ( tolower (sel[0]) == 'q' )
        return new CMainMenu;

    int selection = stoi ( sel );
    m_files = loadFolder(CONFIG.m_mapsFolder);
    sort ( m_files.begin(), m_files.end());

    if ( selection <= 0 || selection > (int)m_files.size())
        throw "Wrong option";
    CGame game;
    if (!game.loadGame( CONFIG.m_mapsFolder + "/" + m_files[selection-1], NEW)) {
        printError("Failed to load game!");
        CMenu::selectOption();
        return new CNewMenu;
    }
    return playGame(game);
}

///Prints load game menu
void CGamesMenu::show() const {
    printFolderMenu(CONFIG.m_gamesFolder);
}

/** Option selector for load game menu,
 * user can type the number of game to load, then game is loaded and started
 * if the game fails to load, error screen is printed
 * @return pointer to appropriate menu ( Main / New / Loser / Winner menu )
 * @throw exception thrown if the input is incorrect
 */
CMenu *CGamesMenu::selectOption() {
    string sel = readInput();
    if ( tolower (sel[0]) == 'q' )
        return new CMainMenu;

    int selection = stoi ( sel );
    m_files = loadFolder(CONFIG.m_gamesFolder);
    sort ( m_files.begin(), m_files.end());

    if ( selection <= 0 || selection > (int)m_files.size())
        throw "Wrong option";
    CGame game;
    if (!game.loadGame( CONFIG.m_gamesFolder + "/" + m_files[selection-1], LOADED)) {
        printError("Failed to load game!");
        CMenu::selectOption();
        return new CGamesMenu;
    }
    return playGame(game);
}

///Prints scores screen
void CScoreMenu::show() const {
    int cnt = 0;
    printBorder();
    printEmptyLine();
    printString("HIGH SCORE:");
    printString("----------------------");
    printEmptyLine();
    ifstream ifs ( CONFIG.m_scoreFile );
    while ( true  ){
        int x;
        ifs >> x;
        if ( ifs.fail() )
            break;
        cnt++;
        printString(to_string(cnt) + ". :  " + to_string(x));
        printEmptyLine();
    }

    int end = DEFAULTHEIGHT - cnt*2 - 6;
    for ( int i = 0; i < end; ++i) {
        if ( i == end - 2) {
            printString("PRESS Q TO GO BACK");
            continue;
        }
        printEmptyLine();
    }
    printBorder();
}

///Prints first manual page
void CManualPage1::show() const {
    string colorPrefixB ("\033[1;");
    string colorPrefix ("\033[");
    int linesNr = 14; //Number of lines going to be printed
    printBorder();
    printEmptyLine();
    printString("-=-=-=-=-=-=-=- ATTACKERS -=-=-=-=-=-=-=-");
    cout << ( CONFIG.m_color ? colorPrefixB + to_string(WARRIOR_COLOR) + "m" : "" );
    printString("Warrior:                                          ");
    cout << ( CONFIG.m_color ? colorPrefix + to_string(WARRIOR_COLOR) + "m" : "" );
    printString("33% chance of blocking attack from artillery      ");
    printString("10% chance of attacking twice                     ");
    cout << ( CONFIG.m_color ? colorPrefixB + to_string(MAGE_COLOR) + "m" : "" );
    printEmptyLine();

    printString("Mage:                                             ");
    cout << ( CONFIG.m_color ? colorPrefix + to_string(MAGE_COLOR) + "m" : "" );
    printString("50% chance of attacking twice                     ");
    printString("10% chance of blocking attack from medic          ");
    printString("10% chance of getting sick and dying when attacked");
    printEmptyLine();

    cout << ( CONFIG.m_color ? colorPrefixB + to_string(ARCHER_COLOR) + "m" : "" );
    printString("Archer                                            ");
    cout << ( CONFIG.m_color ? colorPrefix + to_string(ARCHER_COLOR) + "m" : "" );
    printString("50% chance of extending range by half             ");
    printString("10% chance of attacking twice                     ");
    printEmptyLine();

    if ( CONFIG.m_color ) cout << "\033[0m";
    for ( int i = 0; i < DEFAULTHEIGHT - linesNr -5; ++i){
        printEmptyLine();
    }
    printString("Q - QUIT                       N - NEXT PAGE" );
    printEmptyLine();
    printBorder();

}

///Option selector for first manual page
CMenu *CManualPage1::selectOption() {
    string sel = readInput();
    if ( tolower (sel[0]) == 'q' )
        return new CMainMenu;
    else if ( tolower(sel[0] == 'n'))
        return new CManualPage2;
    else throw "Wrong option";
}

///Prints second manual page
void CManualPage2::show() const {
    string colorPrefixB ("\033[1;");
    string colorPrefix ("\033[");
    int linesNr = 13; //Number of lines going to be printed
    printBorder();
    printEmptyLine();
    printString("-=-=-=-=-=-=-=- TOWERS -=-=-=-=-=-=-=-");
    cout << ( CONFIG.m_color ? colorPrefixB + to_string(MEDIC_COLOR) + "m" : "" );
    printString("Medic:                                               ");
    cout << ( CONFIG.m_color ? colorPrefix + to_string(MEDIC_COLOR) + "m" : "" );
    printString("20% chance of blocking attack coming from Warrior    ");
    printString("33% chance of surviving death and restoring 10 health");
    printString("20% chance of restoring some health after being hit  ");
    printEmptyLine();

    cout << ( CONFIG.m_color ? colorPrefixB + to_string(HEAVY_COLOR-10) + "m" : "" );
    printString("Heavy:                                               ");
    cout << ( CONFIG.m_color ? colorPrefix + to_string(HEAVY_COLOR-10) + "m" : "" );
    printString("20% chance of blocking all attacks                   ");
    printString("10% chance of damage to attacker when hit            ");
    printEmptyLine();

    cout << ( CONFIG.m_color ? colorPrefixB + to_string(ARTILLERY_COLOR) + "m" : "" );
    printString("Artillery                                            ");
    cout << ( CONFIG.m_color ? colorPrefix + to_string(ARTILLERY_COLOR) + "m" : "" );
    printString("25% chance of extending range by half                ");
    printString("20% Chance of double shot                            ");
    if ( CONFIG.m_color ) cout << "\033[0m";
    for ( int i = 0; i < DEFAULTHEIGHT - linesNr -5; ++i){
        printEmptyLine();
    }
    printString("P - PREVIOUS PAGE     Q - MENU     N - NEXT PAGE" );
    printEmptyLine();
    printBorder();
}

///Option selector for second manual page
CMenu *CManualPage2::selectOption() {
    string sel = readInput();
    if ( tolower (sel[0]) == 'q' )
        return new CMainMenu;
    else if ( tolower(sel[0] == 'n'))
        return new CManualPage3;
    else if ( tolower(sel[0] == 'p'))
        return new CManualPage1;
    else throw "Wrong option";
}

///Shows third manual page
void CManualPage3::show() const {
    string colorPrefixB ("\033[1;");
    string colorPrefix ("\033[");
    int linesNr = 10; //Number of lines going to be printed
    printBorder();
    printEmptyLine();
    printString("-=-=-=-=-=-=-=- CONTROLS -=-=-=-=-=-=-=-");
    printString("How to play:                                  ");
    printString("Each attacker has unique symbol and each      ");
    printString("entrance has unique number.                   ");
    printString("Type e.g. '@ 1' to put attacker with symbol @ ");
    printString("to entrance number 1.                         ");
    printEmptyLine();
    printString("-=-=-=-=-=-=-=- GOAL -=-=-=-=-=-=-=-");
    printString("Goal is to get more than half of the attackers");
    printString("to the end ( < ).                             ");
    if ( CONFIG.m_color ) cout << "\033[0m";
    for ( int i = 0; i < DEFAULTHEIGHT - linesNr -5; ++i){
        printEmptyLine();
    }
    printString("P - PREVIOUS PAGE                Q - QUIT" );
    printEmptyLine();
    printBorder();
}

///Option selector for third manual page
CMenu *CManualPage3::selectOption() {
    string sel = readInput();
    if ( tolower (sel[0]) == 'q' )
        return new CMainMenu;
    else if ( tolower(sel[0] == 'p'))
        return new CManualPage2;
    else throw "Wrong option";
}

///Prints winning screen
void CWinner::show() const {
    middleLine("YOU WON!");
}

///Prints losing screen
void CLoser::show() const {
    middleLine("YOU LOST :-( TRY AGAIN");
}

/** Starts menu and lets user to choose
 * changes menu to user selected menu
 * or starts game based on user choice
 */
void CMenuController::start(void){
    while ( true ) {
        system("clear");
        try { currentMenu->show(); }
        catch (...) { throw; }
        CMenu * tmp = NULL;
        try { tmp = currentMenu->selectOption (); }
        catch ( ... ) {
            cout << "WRONG OPTION!" << endl;
            system ("sleep 0.5");
            cin.clear();
            continue;
        }
        cin.clear();
        delete currentMenu;
        currentMenu = tmp;
    }
}
