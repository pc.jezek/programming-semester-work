#include <fstream>
#include <queue>
#include <algorithm>
#include <iomanip>
#include "cmap.h" //also includes entities.h, coords.h, config.h

/// Maximum size of map
const int MAXH = 200, MAXW = 200;

///Constructor, initializes member values
CMap::CMap()
    : m_width ( 0 ), m_height ( 0 ), m_exit (NULL) {
    m_map.resize(MAXH);
    for ( size_t i = 0; i < MAXH; ++i){
        m_map[i].resize(MAXW);
    }
}

/** Loads a map and fills it with elements from file
 * @param filename - file to load
 * @return returns whether the operation succeeded or not
 */
bool CMap::loadMap(const string &filename) {
    m_width = 0; m_height = 0;
    ifstream ifs ( filename );
    if ( ifs . fail() )
        return false;
    string line;
    while ( true ) {
        getline ( ifs, line, '\n' );
        if ( ifs. eof () && ! line.size() ) break;
        if ( ! m_width ) m_width = (unsigned) line.size();
        if ( ! parseLine ( line ) || ifs.fail()) {
            ifs.close();
            return false;
        }
        m_height++;
    }
    ifs.close();
    return ( m_exit &&  m_entrances.size() &&  m_width &&  m_height && findPaths());
}

/** List of Wall characters
 * @param x character to test
 * @return true if character represents wall
 */
bool CMap::isWall(char x) const {
    return      x == '#' || x == '|' || x == '-' || x == '=' || x == '+';
}

/** Determines what symbol loaded from map represents,
 * returns an object of class derived from CEntity,
 * if symbol is entrance / exit stores it into member values for future use
 * @param x character to process
 * @param col column where symbol was found
 * @return entity that is represented by given symbol / nullptr on error
 */
CEntity *CMap::readSymbol( char symbol , int col) {
    CEntrance * ent;
    if ( isWall(symbol)) return new CWall (symbol);
    switch (symbol){
        case ' ':
            return new CFreeSpace(symbol);
        case '<':
            if ( col != 0 ) return nullptr;
            m_exit = new CExit(symbol, m_height, col);
            return m_exit;
        case '_':
            if ( m_entrances.size() > 4 || col != m_width -1)
                return nullptr;
            ent = new CEntrance(symbol, m_height, col);
            m_entrances.push_back(ent);
            return ent;

        default :        return nullptr;
    }
}

/** Parses a string which was read from input file
 * @param line string to process
 * @return false if failed ( size differs or unknown symbol ), true otherwise
 */
bool CMap::parseLine(const string &line) {
    if ( line. size() != (unsigned) m_width )
        return false;
    for ( size_t i = 0; i < line.size(); ++i)
        if ( ! ( m_map[m_height][i] = readSymbol(line[i], (int)i) ) )
            return false;
    return true;
}

/** Prints whole map with numbered entrances and running attackers
 * @param map to print
 */
ostream & operator << ( ostream & os, const CMap & map){
    int entNr = 0;
    size_t AttackerCnt = map.m_attackers.size() / map.m_height + 1;
    auto it = map.m_attackers.begin();
    for ( int i = 0; i < map.m_height; ++i){
        for (int j = 0; j < map.m_width; ++j) {
            map.m_map[i][j]->print(os);
            if ( j == map.m_width - 1) {
                if (entNr < (int) map.m_entrances.size()
                    && map.m_entrances[entNr]->row() == i
                    && map.m_entrances[entNr]->col() == j)
                    os << left << setw(3) << ++entNr;
                else
                    os << setw(3) << " ";
            }
        }
        for ( size_t k = 0; k < AttackerCnt; ++k){
            if ( it == map.m_attackers.end() )
                break;
            os << "\t"; (*it)->print(os); os << " " << (*it)->getHp();
            ++it;
        }
        os << endl;
    }
    os << endl;
    return os;
}

/**
 * Finds a path from [row][col] to end, returns a list of coordinates to get there
 * Throws an exception if end is not reachable from coordinates given as parameters
 * @param row starting position coordinate
 * @param col starting position coordinate
 * @return list of coordinates to get from row,col to end ( m_endRow, m_endCol)
 */
list <TCoord> CMap::pathToEnd(int row, int col) const {
    int                  dstToEnd = 0;
    list   <TCoord>      result;
    vector <vector<int>> beenThere ( (size_t) m_height );
    for ( int i = 0; i < m_height; ++i)
        beenThere[i].resize((size_t) m_width);
    if ( (dstToEnd = searchMapBFS(beenThere, row, col)) == 0)
        throw "End not reachable";
    searchPath(beenThere, result, row, col, 1);

    return result;

}

/**
 * Fills an empty 2D vector beenThere using BFS algorithm, stops when end is reached
 * @param beenThere 2D map of distances
 * @param beenThere 2D map of distances
 * @param sRow starting position row
 * @param sCol starting position end
 * @return distance from start to end ( 0 if not reachable )
 */
int  CMap::searchMapBFS ( vector <vector<int>> & beenThere, int sRow, int sCol) const {
    struct TCoordDst {
        int r;
        int c;
        int d;
        TCoordDst (int row, int col, int dst): r(row), c(col), d (dst){}
    };
    beenThere[sRow][sCol] = 1;
    queue<TCoordDst> q;
    q.push(TCoordDst(sRow,sCol, 1));
    while ( ! q.empty()) {
        TCoordDst tmp = q.front();
        q.pop();
        int row = 0, col = 0;
        for ( int i = 0; i < 4; ++ i){
            switch ( i ){
                case 0 : row = tmp.r + 1; col = tmp.c    ; break;
                case 1 : row = tmp.r - 1; col = tmp.c    ; break;
                case 2 : row = tmp.r    ; col = tmp.c + 1; break;
                case 3 : row = tmp.r    ; col = tmp.c - 1; break;
                default: break;
            }
            if (row < m_height && row >= 0
                && col < m_width && col >= 0
                && m_map[row][col]->discover()
                && beenThere[row][col] == 0) {
                   beenThere[row][col] = tmp.d + 1;
                if ( row == m_exit->row() && col == m_exit->col())
                    return beenThere [ m_exit->row() ] [ m_exit->col() ];
                q.push(TCoordDst(row, col, tmp.d + 1));
            }
        }
    }
    return beenThere [ m_exit->row() ] [ m_exit->col() ];
}

/** Finds actual path and fills list with coordinates using a map filled by BFS
 * @param beenThere 2D map of distances filled by BFS
 * @param result list where the resulting path is stored
 * @param row current position in recursion - coordinate
 * @param col current position in recursion - coordinate
 * @param dst current distance in recursion - coordinate
 * @return list filled with coordinates, empty if not reachable ( shouldn't occur! )
 */
bool CMap::searchPath(vector<vector<int>> &beenThere, list <TCoord> &result, int row, int col, int dst) const {
    int tmprow = m_exit->position().t_row;
    int tmpcol = m_exit->position().t_col;
    result.push_front(TCoord(tmprow,tmpcol));
    dst = beenThere[tmprow][tmpcol];
    while ( true ){
        dst--;
        if ( tmprow == row && tmpcol == col-1 ) {
            result.push_front(TCoord(tmprow, tmpcol));
            return true;
        }
        int newRow = 0, newCol = 0;
        for ( int i = 0; i < 4; ++i){
            switch (i){
                case 0:
                    newRow = tmprow + 1;
                    newCol = tmpcol;
                    break;
                case 1:
                    newRow = tmprow - 1;
                    newCol = tmpcol;
                    break;
                case 2:
                    newRow = tmprow;
                    newCol = tmpcol + 1;
                    break;
                case 3:
                    newRow = tmprow;
                    newCol = tmpcol - 1;
                    break;
                default: break;
            }
            if ( newRow >= 0 && newRow < m_height && newCol >= 0 && newCol < m_width
                 && beenThere[newRow][newCol] == dst){
                tmprow = newRow;
                tmpcol = newCol;
                result.push_front(TCoord ( tmprow, tmpcol));
                break;
            }
        }
    }
}

/**
 * Finds path to exit for every start in map, stores it into relevant CEntrance
 * Replaces CFreeSpace by CPath in map
 * @return success / not
 */
bool CMap::findPaths() {
    list < TCoord > pathList;
    for ( const auto & x : m_entrances ) {
        try {
            list <TCoord> path = pathToEnd(x->row(), x->col());
            x->setPath(pathToEnd(x->row(), x->col()) );
            for ( auto it = ++path.begin(); it != path.end(); ++it) {
                pathList.push_back(*it);
            }

        } catch (...) {
            return false;
        }
    }
    for ( const auto & x : pathList){
        if ( x == m_exit->position())
            continue;
        delete m_map [x.t_row][x.t_col];
        m_map [x.t_row][x.t_col] = new CPath (' ');
    }
    return true;
}


/**
 * Add attacker to entrance specified by second parameter
 * @param attacker object to add to map
 * @param entranceNr where to place him
 * @return success / not
 */
bool CMap::addAttacker(const CAttacker & attacker, int entranceNr) {
    int entranceIdx = entranceNr - 1;
    if ( entranceIdx < 0 || entranceIdx >= (int)m_entrances.size())
        return false;

    int posRow = (++m_entrances[entranceIdx]->getPath().begin())->t_row;
    int posCol = (++m_entrances[entranceIdx]->getPath().begin())->t_col;
    if ( ! m_map[posRow][posCol]->discover())
        return false;

    CAttacker * newAttacker = attacker.clone();
    newAttacker->setPath(m_entrances[entranceIdx]->getPath() );
    newAttacker->setEntrance(entranceNr);
    delete m_map[posRow][posCol];

    m_map[posRow][posCol] = newAttacker;
    m_attackers.push_back(newAttacker);

    return true;
}

/**Adds tower to map
 * @param tower to put in map
 * @param row coordinate
 * @param col coordinate
 * @return success / not
 */
bool CMap::addTower(const CTower & tower, int row, int col) {
    delete m_map[row][col];
    CTower * newTower = tower. clone ();
    newTower->setPos(TCoord(row,col));
    m_map[row][col] = newTower;
    m_towers.push_back(newTower);
    return true;
}

/**
 * Moves all attackers,
 * except those, who are currently sleeping ( their speed is slower than maximum speed and it hasn't been reset yet).
 * @param maxSp speed of the fastest attacker on map
 * @return points if attacker got to the end, 0 otherwise
 */
int CMap::move(int maxSp) {
    int score = 0;
    for ( auto attacker = m_attackers.begin(); attacker != m_attackers.end(); ++attacker ){
            TCoord next = (*attacker)->nextMove(maxSp);
            TCoord pos = (*attacker)->position();
            if (next == pos || !m_map[next.t_row][next.t_col]->discover())
                continue;
            if (next == m_exit->position()) {
                score++;
                delete m_map[pos.t_row][pos.t_col];
                *attacker = NULL;
                m_map[pos.t_row][pos.t_col] = new CPath(' ');
                continue;
            }
            CEntity *tmp;
            tmp = m_map[pos.t_row][pos.t_col];
            m_map[pos.t_row][pos.t_col] = m_map[next.t_row][next.t_col];
            m_map[next.t_row][next.t_col] = tmp;
            (*attacker)->move();
    }
    vector<CAttacker*> newAttackers;
    copy_if ( m_attackers.begin(), m_attackers.end(), back_inserter(newAttackers), [] (CAttacker * att) { return att != NULL; });
    m_attackers = newAttackers;
    return score;
}

/**
 * Sets speed counter of all attackers to 0
 * @return speed of the fastest attacker
 */
int CMap::resetSpeed() {
    int max = 0;
    for ( auto it = m_attackers.begin(); it != m_attackers.end(); ++it){
        if ( (*it) -> speed() > max )
            max = (*it)->speed();
        (*it)->resetSpeedCnt();
    }
    return max;
}

/**
 * AI which displaces towers at random across the game map. Towers never block attacker's path to end.
 * @param towers Vector of available towers along with information how many to displace
 * @return success / not
 */
bool CMap::displaceTowers(const vector<pair <CTower*, int>> &towers) {
    vector <TCoord> availablePositions;
    if ( towers.empty() ) {
        return false;
    }
    for ( int i = 0; i < m_height; ++i)
        for ( int j = 0; j < m_width; ++j){
            if ( m_map[i][j]->discover() && ! m_map[i][j]->isPath() && ! ( i == m_exit->row() && j == m_exit->col() ) )
                availablePositions.push_back(TCoord (i, j));
        }
    for ( const auto & x: towers) {
        for (int i = 0; i < x.second; ++i) {
            size_t maxNr;
            if (!(maxNr = availablePositions.size())) {
                return false;
            }
            srand((unsigned) time(NULL));
            size_t randCoord = (rand() % maxNr);
            TCoord dest = availablePositions[randCoord];
            availablePositions.erase(availablePositions.begin() + randCoord);
            delete m_map[dest.t_row][dest.t_col];
            CTower * newTower = x.first->clone();
            newTower->setPos(dest);
            m_map[dest.t_row][dest.t_col] = newTower;
            m_towers.push_back(newTower);
        }
    }
    return true;
}

/** Performs an attack on every object in each attacker's / tower's distance
 * deletes an attacker/tower if he/it dies
 * @param towers - who is being attacked ( true - towers / false - attackers )
 */
void CMap::attack ( bool towers ){
    bool isDelAttackers = false, isDelTowers = false;
    for ( auto & attacker : m_attackers ){
        for ( auto & tower : m_towers){
            if ( !attacker || ! tower) continue;
            if ( towers    && ! attacker->hasInRange(tower->position())) continue;
            if ( !towers   && ! tower->hasInRange(attacker->position())) continue;
            towers ? attacker->attack(*tower) : tower->attack(*attacker);
            if (attacker->getHp() <= 0) {
                isDelAttackers = true;
                int toDelR = attacker->position().t_row;
                int toDelC = attacker->position().t_col;
                delete m_map[toDelR][toDelC];
                m_map[toDelR][toDelC] = new CPath(' ');
                attacker = NULL;
            }
            if (tower->getHp() <= 0) {
                isDelTowers = true;
                int toDelR = tower->position().t_row;
                int toDelC = tower->position().t_col;
                delete m_map[toDelR][toDelC];
                m_map[toDelR][toDelC] = new CFreeSpace(' ');
                tower = NULL;
            }
        }
    }
    if ( isDelAttackers ){
        vector<CAttacker*> newAttackers;
        copy_if ( m_attackers.begin(), m_attackers.end(), back_inserter(newAttackers), [] (CAttacker * att) { return att != NULL; });
        m_attackers = newAttackers;
    }
    if ( isDelTowers ){
        vector<CTower *> newTowers;
        copy_if ( m_towers.begin(), m_towers.end(), back_inserter(newTowers), [] (CTower * tow) { return tow != NULL; });
        m_towers = newTowers;
    }
}

/** Adds attacker to the map, used while loading game
 * @param attacker source attacker to place in map
 * @param entranceNr where the attacker started his journey
 * @param hp how much health has he left
 * @param steps how many steps has he taken since he entered the map
 * @return success / not
 */
bool CMap::addAttacker(const CAttacker &attacker, int entranceNr, int hp, int steps) {
    if ( ! addAttacker(attacker, entranceNr) )
        return false;
    CAttacker * newAttacker = m_attackers[m_attackers.size()-1];
    TCoord pos = newAttacker->position();
    m_map[pos.t_row][pos.t_col] = new CPath(' ');
    newAttacker -> setHp(hp);
    newAttacker -> setEntrance(entranceNr);
    for ( int i = 0; i < steps; ++i) {
        newAttacker -> resetSpeedCnt();
        newAttacker->move();
    }
    pos = newAttacker->position();
    if (! m_map[pos.t_row][pos.t_col] -> discover())
        return false;
    delete m_map[pos.t_row][pos.t_col];
    m_map[pos.t_row][pos.t_col] = newAttacker;
    return true;
}

/** Saves every attacker on map to file
 * @param ofs stream to save to
 * @return false when save fails
 */
bool CMap::saveAttackers(ofstream &ofs) {
    for ( const auto & attacker: m_attackers)
        attacker->save(ofs);
    return ofs.fail();
}

/** Saves every tower on map to file
 * @param ofs stream to save to
 * @return false when save fails
 */
bool CMap::saveTowers(ofstream &ofs) {
    for ( const auto & tower : m_towers )
        tower->save(ofs);
    return ofs.fail();
}

///destructor, deletes allocated memory
CMap::~CMap() {
    for ( size_t i = 0; i < m_map.size(); ++i){
        for ( size_t j = 0; j < m_map[i].size(); ++j)
            delete m_map[i][j];
    }

}