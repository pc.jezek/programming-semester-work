#include <iomanip>
#include <time.h>
#include "attackers.h" //also include towers.h, entities.h, coords.h, config.h

/** Generates random number and returns true if given chance has been reached */
bool calculateChance (int chance){
    srand((unsigned) time(NULL));
//    srand(1);
    int randNr = (rand() % chance);
    return randNr == 1;
}

//-------------------------------- WARRIOR ---------------------------------//
/** Warrior's special ability,
 * sometimes warrior can attack twice,
 * @param dst tower to attack
 */
void CWarrior::attack(CTower &dst) {
    dst.defend(*this);
    if ( calculateChance(m_twiceChance))
        dst.defend(*this);
}

/** Warrior's special ability,
 * sometimes warrior can block artillery's attack
 * @param src tower attacking
 */
void CWarrior::defend(CArtillery &src) {
    if ( calculateChance(m_blockingChance) )
        return;
    CAttacker::defend(src);
}

/** Print warrior's details */
void CWarrior::printDetails(ostream &os) const {
    if (CONFIG.m_color) os <<"\033[1;" << WARRIOR_COLOR << "m";
    os << setw(10) << "WARRIOR ";
    if (CONFIG.m_color) os << "\033[0m";
    CAttacker::printDetails(os);
}
/** Print warrior's symbol */
void CWarrior::print(ostream &os) const {
    if (CONFIG.m_color) os <<"\033[1;" << WARRIOR_COLOR << "m";
    CAttacker::print(os);
    if (CONFIG.m_color) os << "\033[0m";
}

//-------------------------------- ARCHER ---------------------------------//

/** Archers's special ability
 * sometimes archer can attack twice
 * @param dst tower attacking
 */
void CArcher::attack(CTower &dst) {
    dst.defend(*this);
    if ( calculateChance(m_twiceChance))
        dst.defend(*this);
}

/** Archer's special ability
 * sometimes, archer can extend it's range
 * @param src coordinates to compare
 * @return if the coordinates are in archer's reach
 */
bool CArcher::hasInRange( const TCoord & src ) const {
    return  abs( src.t_col - m_pos.t_col) <=
            ( m_range  * (calculateChance(m_extendedDistanceChance)  ? 1.5 : 1.0))
            && abs( src.t_row - m_pos.t_row) <=
               ( m_range  * (calculateChance(m_extendedDistanceChance) ? 1.5 : 1.0)) ;
}

/** Print archer's details */
void CArcher::printDetails(ostream &os) const {
    if (CONFIG.m_color) os <<"\033[1;" << ARCHER_COLOR << "m";
    os << setw(10) << "ARCHER ";
    if (CONFIG.m_color) os << "\033[0m";
    CAttacker::printDetails(os);
}

/** Print archer's symbol */
void CArcher::print(ostream &os) const {
    if (CONFIG.m_color) os <<"\033[1;" << ARCHER_COLOR << "m";
    CAttacker::print(os);
    if (CONFIG.m_color) os << "\033[0m";
}

//-------------------------------- MAGE ---------------------------------//
/** Print mage's details */
void CMage::printDetails(ostream &os) const {
    if (CONFIG.m_color) os <<"\033[1;" << MAGE_COLOR << "m";
    os << setw(10) << "MAGE ";
    if (CONFIG.m_color) os << "\033[0m";
    CAttacker::printDetails(os);
}

/** Print mage's symbol */
void CMage::print(ostream &os) const {
    if (CONFIG.m_color) os <<"\033[1;" << MAGE_COLOR << "m";
    CAttacker::print(os);
    if (CONFIG.m_color) os << "\033[0m";
}


/** Mage's special ability
 * sometimes, mage can attack twice
 * @param tower to attack
 */
void CMage::attack(CTower &dst) {
    dst.defend(*this);
    if ( calculateChance(m_twiceChance))
        dst.defend(*this);
}

/** Mage's special ability
 * sometimes, mage can die after being hit
 * @param src
 */
void CMage::defend(CTower &src) {
    CAttacker::defend(src);
    if (calculateChance(m_dyingChance))
        m_hp = 0;
}

/** Mage's special ability
 * sometimes, mage can block attack from medic
 * @param src medic attacking
 */
void CMage::defend(CMedic &src) {
    if ( calculateChance(m_blockingChance))
        return;
    CAttacker::defend(src);
}

