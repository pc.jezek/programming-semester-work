#ifndef V1_1_MENU_H
#define V1_1_MENU_H
#define DEFAULTWIDTH    66
#define DEFAULTHEIGHT   20
#define CENTER           1
#define LEFT             0
#define RIGHT            2
#define NEW           true
#define LOADED       false

#include <climits>
#include "game.h"   // also includes cmap.h, entities.h, coords.h, config.h
#include <iostream>
#include <vector>

///Object used for exception only
class CantOpenFile{};


/** Menu class
 * Abstract class with a basic set of methods to draw parts of menu.
 * Show and select methods are implemented in inherited classes based on menu type.
 */
class CMenu {
protected:
    string                 readInput       ( void ) const;
    void                   printError      ( const string & error) const;
    void                   printEmptyLine  ( int lines = 1) const;
    void                   printBorder     () const;
    void                   printString     ( const string & str, int position = CENTER) const;
    void                   middleLine      ( const string & line) const;
    void                   printFolderMenu ( const string & folder) const;
    CMenu *                playGame        ( CGame & game);
    int                    printFolder     ( const string & folder ) const;
    vector<string>         loadFolder      ( const string & folder ) const;
public:
    virtual ~CMenu() = default;
    virtual CMenu * selectOption ( );
    virtual void    show ( ) const = 0;
};

///Main menu
class CMainMenu   : public CMenu {
public:
    void    show ( ) const override;
    CMenu * selectOption ( ) override;
};

///New game menu
class CNewMenu  : public CMenu {
public:
    CMenu * selectOption ( ) override;
    void    show ( ) const override;
protected:
    vector <string> m_files;
};

///Load game menu
class CGamesMenu  : public CMenu {
public:
    void    show ( ) const override;
    CMenu * selectOption ( ) override;
protected:
    vector <string> m_files;
};

///Show score menu
class CScoreMenu : public CMenu {
public:
    void    show ( ) const override;
};

///First page of manual
class CManualPage1  : public CMenu {
public:
    void    show ( ) const override;
    CMenu * selectOption ( ) override;
};

///Second page of manual
class CManualPage2  : public CMenu {
public:
    void    show ( ) const override;
    CMenu * selectOption ( ) override;
};

///Third page of manual
class CManualPage3  : public CMenu {
public:
    void    show ( ) const override;
    CMenu * selectOption ( ) override;
};

///Show winning screen
class CWinner  : public CMenu {
public:
    void    show ( ) const override;
};

///Show losing screen
class CLoser  : public CMenu {
public:
    void    show ( ) const override;
};

///Handles transitions between menu screens and user input
class CMenuController {
protected:
    CMenu      * currentMenu;
public:
    ///Sets main menu as current menu
    CMenuController ( ) : currentMenu ( new CMainMenu ){}
    CMenuController ( const CMenuController & src ) = delete;
    void operator = ( const CMenuController & src ) = delete;
    ~CMenuController(){ delete currentMenu; }
    void start ( void );
};

#endif //V1_1_MENU_H
